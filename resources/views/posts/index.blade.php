@extends('layouts.app')

@section('content')
    <h1 class="text-center">Posts</h1>
    @if (count($posts) > 0)
        @foreach ($posts as $post)
        <div class="card">
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <img src="/storage/cover_images/{{ $post->cover_image }}" style="width: 40%;">
                    <br>
                </div>
                <div class="col-md-8 col-sm-8">
                     <div class="card-block">
                        <h3 class="card-title"><a href="/posts/{{ $post->id }}">{{ $post->title }}</a></h3>
                        <small class="card-text">Written on {{ $post->created_at }} by {{ $post->user->name }}</small>
                    </div>
                </div>
            </div>
        </div>
        <br>
        @endforeach
        {{$posts->links()}}
    @else
        <p>No posts found</p>
    @endif
@endsection